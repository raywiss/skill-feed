# OpsDroid skill to parse feeds

## Skill config
```
## Skill modules
skills:
  feed:
    repo: https://gitlab.com/raywiss/skill-feed.git
    connector: "matrix"
```