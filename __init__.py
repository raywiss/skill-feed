from opsdroid.skill import Skill
from opsdroid.matchers import match_regex, match_crontab
from opsdroid.constraints import constrain_rooms
from opsdroid.events import Message
from copy import copy
import datetime
import time
import logging
import feedparser
import re


def msg(feed,entry,need_description):
    try:
        link=entry.link
    except:
        return "No link"
    try:
        feed_title=feed.title
    except:
        feed_title="Feed Title"
    try:
        entry_title=entry.title
    except:
        entry_title=link
    txt = "[{}] <a href=\"{}\">{}</a>".format(feed_title,link, entry_title)
    if need_description:
        try:
            txt='<details> <summary> '+txt+' </summary> <br/> '+entry.description+' </details>'
        except:
            pass
    return str(txt)


def structtime2datetime(st):
    return datetime.datetime(st.tm_year, st.tm_mon, st.tm_mday, st.tm_hour, st.tm_min, st.tm_sec, 0, tzinfo=datetime.timezone.utc)


class Feed(Skill):

    def __init__(self, opsdroid, config):
        super(Feed, self).__init__(opsdroid, config)
        # do some setup stuff here
        logging.debug("Loaded feed module")

    @match_regex(r'[Hh]elp feed')
    async def help_feeds(self, message):
        txt = """<h1>Feed Skill Syntax</h1>

    <ul>
    <li><code>Help feed</code> - This help.</li>
    <li><code>Add feed {url} [with description ]to {room}</code> - Add a feed by URL</li>
    <li><code>Del feed {url}</code> - Delete the feed by URL</li>
    <li><code>List feeds</code> - List the added feeds</li>
    <li><code>Read feeds</code> - Read news from the listed feeds.</li>
    </ul>
    <h1>Setup</h1>

    You need a "feedmaster" room. To configure the feeds.
    """
        await message.respond(txt)

    @match_regex(r'[Ll]ist feed')
    async def list_feeds(self, message):
        feeds = await self.opsdroid.memory.get("feeds")
        if feeds == None:
            await message.respond("You have no feeds yet.")
        else:
            txt = "<table><tr><th>URL</th><th>Room</th><th>Last seen</th><th>Desc.</th></tr>"
            for feed_name in feeds:
                room = str(feeds[feed_name]['room'])
                if hasattr(message, "connector") and hasattr(message.connector, "lookup_target"):
                    rooms = list(
                        map(message.connector.lookup_target, [room, 'feedmaster']))
                else:
                    rooms = [room]
                try:
                    need_description=feeds[str(feed_name)]["description"]
                except:
                    need_description='False'
                if message.target in rooms:
                    txt += "<tr><td>{}</td><td>{}</td><td>{}</td><td>{}</td></tr>".format(
                        str(feed_name), feeds[str(feed_name)]["room"], feeds[str(feed_name)]["last"],
                        need_description)
            txt += "</table>"
            await message.respond(txt)

    @match_regex(r'[Aa]dd feed (?P<feed_name>.*) to (?P<room_name>.*)$')
    @constrain_rooms(['feedmaster'])
    async def add_feed(self, message):
        feed_name = message.regex.group('feed_name')
        url_and_desc=re.search(r'^(?P<url>.*) with description *$',feed_name)
        if url_and_desc:
            feed_name=url_and_desc.group("url")
            need_description=True
        else:
            need_description=False
        room_name = message.regex.group('room_name')
        feeds = await self.opsdroid.memory.get("feeds")
        if feeds == None:
            feeds = {}
        feeds[str(feed_name)] = {
            "last": str(datetime.datetime.now(datetime.timezone.utc)),
            "room": str(room_name),
            "description": str(need_description)
        }
        await self.opsdroid.memory.put("feeds", feeds)
        await message.respond("Feed added at {}.".format(feeds[str(feed_name)]))

    @match_regex(r'[Dd]el feed (?P<feed_name>.*)$')
    @constrain_rooms(['feedmaster'])
    async def del_feed(self, message):
        feed_name = message.regex.group('feed_name')
        feeds = await self.opsdroid.memory.get("feeds")
        if feeds == None:
            feeds = {}
        del feeds[str(feed_name)]
        await self.opsdroid.memory.put("feeds", feeds)
        await message.respond("Feed removed.")

    @match_regex(r'[Rr]ead feeds')
    async def read_feeds(self, message):
        state_changed = False
        feeds = await self.opsdroid.memory.get("feeds")
        if feeds == None:
            await message.respond("You have no feeds yet.")
        else:
            for feed_name in feeds:
                try:
                    last_seen = datetime.datetime.strptime(
                        str(feeds[feed_name]['last']), '%Y-%m-%d %H:%M:%S.%f%z')
                except:
                    last_seen = datetime.datetime.strptime(
                        str(feeds[feed_name]['last']), '%Y-%m-%d %H:%M:%S%z')
                room = str(feeds[feed_name]['room'])
                try:
                    need_description=True if str(feeds[feed_name]['description']) == 'True' else False
                except:
                    need_description=False
                if hasattr(message, "connector") and hasattr(message.connector, "lookup_target"):
                    rooms = list(
                        map(message.connector.lookup_target, [room, 'feedmaster']))
                else:
                    rooms = [room]
                if message.target in rooms:
                    await message.respond("Reading: [{}] from: [{}]".format(feed_name, last_seen))
                    feed = feedparser.parse(feed_name)
                    last_entry = copy(last_seen)
                    for entry in feed.entries:
                        try:
                            last_parsed = structtime2datetime(entry.published_parsed)
                        except:
                            last_parsed = structtime2datetime(entry.updated_parsed)
                        if last_entry < last_parsed:
                            last_entry = copy(last_parsed)
                        if last_parsed > last_seen:
                            await self.opsdroid.send(
                                Message(
                                    text=msg(feed.feed,entry,need_description),
                                    connector=self.config.get("connector"),
                                    target=room
                                )
                            )
                    if last_seen < last_entry:
                        feeds[str(feed_name)]['last'] = str(last_entry)
                        state_changed = True
                    time.sleep(2.4)
            if state_changed:
                await self.opsdroid.memory.put("feeds", feeds)

    @match_crontab("*/15 6-21 * * *", timezone="Europe/Budapest")
    async def cronread_feeds(self, event):
        state_changed = False
        feeds = await self.opsdroid.memory.get("feeds")
        if feeds:
            for feed_name in feeds:
                try:
                    last_seen = datetime.datetime.strptime(
                        str(feeds[feed_name]['last']), '%Y-%m-%d %H:%M:%S.%f%z')
                except:
                    last_seen = datetime.datetime.strptime(
                        str(feeds[feed_name]['last']), '%Y-%m-%d %H:%M:%S%z')
                room = str(feeds[feed_name]['room'])
                try:
                    need_description=True if str(feeds[feed_name]['description']) == 'True' else False
                except:
                    need_description=False
                feed = feedparser.parse(feed_name)
                last_entry = copy(last_seen)
                for entry in feed.entries:
                    try:
                        last_parsed = structtime2datetime(entry.published_parsed)
                    except:
                        last_parsed = structtime2datetime(entry.updated_parsed)
                    if last_entry < last_parsed:
                        last_entry = copy(last_parsed)
                    if last_parsed > last_seen:
                        await self.opsdroid.send(
                            Message(
                                text=msg(feed.feed,entry,need_description),
                                connector=self.config.get("connector"),
                                target=room
                            )
                        )
                if last_seen < last_entry:
                    feeds[str(feed_name)]['last'] = str(last_entry)
                    state_changed = True
                time.sleep(3.4)
            if state_changed:
                await self.opsdroid.memory.put("feeds", feeds)
